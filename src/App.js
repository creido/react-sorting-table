import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import papa from 'papaparse'

import Table from './components/Table/Table'
import getData from './services/getData'

import {scrubNumbers, scrubLineBreaks, formatDataHeaders} from './utils/dataFormatting'

class App extends Component {
  state = {
    citiesData: {},
  }

  getTableData() {
    // TODO: tidy up, abstract and streamline the 'data formatting' process
    getData('/cities.csv')
      .then(text => {
        // replace # with id so we can use Router to get specific pathname
        const cleanText = scrubLineBreaks(scrubNumbers(text)).replace('#', 'id')

        /**
         * TODO: future dev - capture non lowercase headers before formatting and create
         * an array of object literals to describe the headers and preserve original text
         * e.g.
         * headers = [
         *   {key: "id", name: "#"},
         *   {key: "city", name: "City"},
         *   ...
         * ]
         */

        // CSV parsing
        const data = papa.parse(cleanText, {
          beforeFirstChunk: chunk => formatDataHeaders(chunk),
          // parse data as an array of objects to preserve keys
          header: true,
        })

        //cherry pick the data we need
        const newData = {
          headers: data.meta.fields,
          rows: data.data
        }

        // update state
        this.setState(prevState => {
          return {
            citiesData: {
              ...prevState.citiesData,
              ...newData
            }
          }
        })
      })
      .catch(err => console.warn('OH NO!: ' + err))
  }

  componentDidMount() {
    this.getTableData()
  }

  render() {
    return (
      <div className="App">
        <Router>
          <main>
            <Table data={this.state.citiesData} />
          </main>
        </Router>
      </div>
    );
  }
}

export default App;
