import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'

import TableHead from './TableHead'
import TableBody from './TableBody'
import dynamicSort from '../../utils/dynamicSort'

import './table.scss'

class Table extends Component {
  // part of potential work for introducing Context API
  // static TableHead = TableHead
  // static TableBody = TableBody

  state = {
    activeHeader: '',
    headers: [],
    rows: [],
  }

  /**
   * TODO: future dev - move this method into component state to be accessible through context
   * <th> elements can have click handlers which would call this function direct without
   */
  sortByHeader(header) {
    this.setState(prevState => {

      if (header !== prevState.activeHeader) {
        // set order on selected column
        return {
          activeHeader: header,
          rows: prevState.rows.slice().sort(dynamicSort(header))
        }
      }

      /**
       * TODO: future dev - for flipping the sort order
       */
      // ...
      // } else {
      //   // simply reverse order if header is already active
      //   // i.e. data has already been sorted by this header
      //   return {
      //     rows: prevState.rows.slice().reverse()
      //   }
      // }

    })
  }

  setData() {
    this.setState((prevState, props) => {
      const {headers, rows} = props.data

      return {
        headers: [...prevState.headers, ...headers],
        rows: [...prevState.rows, ...rows],
      }
    })
  }

  getActiveHeaderFromUrl(url) {
    const header = decodeURIComponent(url.substring(1)).toLowerCase().replace(/-/g, ' ')

    return this.sortByHeader(header)
  }


  componentDidUpdate(prevProps) {
    if (this.props.data !== prevProps.data) {
      this.setData()

      // location can be read from props as provided by `withRouter` HOC
      this.getActiveHeaderFromUrl(this.props.location.pathname)
    }
  }

  render() {
    const {headers, rows} = this.state

    return (
      // TODO: future dev - if introducing context wrap <table> element in the Context Provider
      // <TableContext.Provider value={this.state}>
      <table>
        <TableHead headers={headers} />
        <TableBody rows={rows} />
      </table>
    )
  }
}

export default withRouter(Table)
