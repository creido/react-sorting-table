import React from 'react'

import TableRow from './TableRow'

const TableBody = ({rows}) =>
  <tbody>
    {
      rows && rows.map((row, index) =>
        <TableRow key={index} row={row} />
      )
    }
  </tbody>

export default TableBody
