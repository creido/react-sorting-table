import {createContext} from 'react'

const defaultState = {}

export const TableContext = createContext(defaultState)
