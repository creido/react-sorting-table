import React from 'react'

import TableHeaderCell from './TableHeaderCell'

const TableHead = ({headers}) =>
  <thead>
    <tr>
      {
        headers && headers.map(header =>
          <TableHeaderCell
            key={header}>{header}</TableHeaderCell>
        )
      }
    </tr>
  </thead>


export default TableHead
