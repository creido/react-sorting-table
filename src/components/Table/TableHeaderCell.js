import React from 'react'

// TODO: future dev - use context to supply element text, access to sortByHeader function
// and activeHeader property to enable styling of active column
// import {TableContext} from './TableContext'

const TableHeaderCell = ({children}) =>
  <th scope="col">{children}</th>

export default TableHeaderCell
