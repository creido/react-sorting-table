import React from 'react'

import TableCell from './TableCell'

const TableRow = ({row}) =>
  <tr>
    {
      Object.keys(row).map(key =>
        <TableCell key={key}>{row[key]}</TableCell>
      )
    }
  </tr>

export default TableRow
