/**
 * GET text data from a given API
 *
 * @param   url            {string}  GET url of the API call
 * @param   fetchFunction  {string}  Name of the fetch method to use
 * @returns {Promise<any>}
 */
export const getData = (url, fetchFunction = window.fetch) => {
  return fetchFunction(url)
    .then(response => response.ok ? response.text() : Promise.reject(response.status))
    .catch(err => {
      throw new Error('error')
    })
}

export default getData
