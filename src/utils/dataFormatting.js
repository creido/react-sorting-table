import {regexNumberStrings, regexLineBreaks} from './regexPatterns'

export const scrubNumbers = txt => txt.replace(regexNumberStrings, str => {
  return str.replace(/,/g, '')
})

export const scrubLineBreaks = txt => txt.replace(regexLineBreaks, str => {
  return str.replace(/(\r\n|\r|\n)/g, ' ')
})

export const formatDataHeaders = chunk => {
  const rows = chunk.split( /\r\n|\r|\n/ );
  const headings = rows[0].toLowerCase();

  // headings set to lowercase to allow for easy case-insensitive matching
  rows[0] = headings;
  return rows.slice().join('\r\n');
}
