const compareOptions = {
  ignorePunctuation: true,
  numeric: true,
  sensitivity: 'base',
}

/**
 * Function to sort alphabetically an array of objects by a specified key.
 *
 * @param {String} key  Key of the object to sort.
 */
export const dynamicSort = (key, locale = 'en') => {
  let sortOrder = 1

  if (key[0] === "-") {
    sortOrder = -1
    key = key.substr(1)
  }

  return (a, b) => {
    if (!a[key] || !b[key]) return

    if (sortOrder === -1) {
      return b[key].localeCompare(a[key], locale, compareOptions)
    } else {
      return a[key].localeCompare(b[key], locale, compareOptions)
    }
  }
}

export default dynamicSort
