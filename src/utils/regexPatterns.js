export const regexNumberStrings = new RegExp(/"\d{1,3}(,\d{3})*"/g)

export const regexLineBreaks = new RegExp(/"\w*(\r\n|\r|\n)\w*"/g)
